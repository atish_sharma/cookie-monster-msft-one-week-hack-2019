# Cookie Monster #

### Microsoft One Week Hack 2019 ###

### Install node modules: ###
```
npm install --save
```

### Install http-server globally: ###
```
npm install http-server -g
```

### Serve folder on given port from root of project: ###
```
http-server -p <PORT>
```

### Open in browser on given URL: ###
```
http://localhost:<PORT>
```