const TAG = "INDEX";
const DEBUG = false;

const Logger = {
    log: function(tag, data) {
        if (!DEBUG) {
            return;
        }

        console.log(tag, data);
    }
};

const UTILS = {
    useless: function(obj) {
        return ((null == obj) || (undefined == obj));
    }
};

const CAMERA = {
    stream: null,
    canvas: null,
    ctx: null,
    recordedFoodStuff: [], // ["banana", "milk", "butter"],
    recordedFoodMap: {},
    clearFoodRecord: function() {
        CAMERA.recordedFoodStuff = [];
        CAMERA.recordedFoodMap = {};
    },
    recordFood: function(obj, img) {
        if (UTILS.useless(obj)) {
            return;
        }

        for (let o of obj) {
            let currObj = o;
            while (!UTILS.useless(currObj)) {
                if (currObj.object.toLowerCase() == "food") {
                    let foodName = o.object;
                    let foodKey = foodName.toLowerCase();
                    if (UTILS.useless(CAMERA.recordedFoodMap[foodKey])) {
                        CAMERA.recordedFoodStuff.push(foodKey);
                        CAMERA.recordedFoodMap[foodKey] = {
                            "img_data": img,
                            "rectangle": o.rectangle
                        }
                    }
                    M.toast({ html: `${foodName} found.` });
                    break
                }
                currObj = currObj.parent;
            }

            populateFoodCount();
        }

        Logger.log(TAG, CAMERA.recordedFoodStuff);
        Logger.log(TAG, CAMERA.recordedFoodMap);
    },
    initCamera: function() {
        Logger.log(TAG, "initCamera");

        CAMERA.canvas = document.getElementById("img-canvas");
        CAMERA.ctx = CAMERA.canvas.getContext("2d");
        CAMERA.video = document.getElementById("video");

        $("#video").on("click", function() {
            CAMERA.getImageData(async function(blob) {
                showPreloader(true);
                await processImage(blob);
                showPreloader(false);
            });
        });
    },
    getImageData: function(next) {
        Logger.log(TAG, "getImageData");

        const NEXT = next;

        CAMERA.ctx.drawImage(video, 0, 0, CAMERA.canvas.width, CAMERA.canvas.height);
        CAMERA.canvas.toBlob(function(blob) {
            NEXT(blob);
        });
    }
};

const RECIPES_LIST = { "count": 30, "recipes": [{ "publisher": "The Pioneer Woman", "f2f_url": "http://food2fork.com/view/46956", "title": "Deep Dish Fruit Pizza", "source_url": "http://thepioneerwoman.com/cooking/2012/01/fruit-pizza/", "recipe_id": "46956", "image_url": "http://static.food2fork.com/fruitpizza9a19.jpg", "social_rank": 100.0, "publisher_url": "http://thepioneerwoman.com" }, { "publisher": "Serious Eats", "f2f_url": "http://food2fork.com/view/367438", "title": "How to Make Peepshi = Peeps Sushi", "source_url": "http://www.seriouseats.com/recipes/2010/03/peeps-recipes-how-to-make-peepshi-sushi-rice-krispies-treats-easter.html", "recipe_id": "367438", "image_url": "http://static.food2fork.com/22100331peepshiprimarydb4a.jpg", "social_rank": 100.0, "publisher_url": "http://www.seriouseats.com/" }, { "publisher": "Simply Recipes", "f2f_url": "http://food2fork.com/view/36482", "title": "How to Make Fruit Leather", "source_url": "http://www.simplyrecipes.com/recipes/how_to_make_fruit_leather/", "recipe_id": "36482", "image_url": "http://static.food2fork.com/fruitleather300x2001f9f84c4.jpg", "social_rank": 99.99999999999989, "publisher_url": "http://simplyrecipes.com" }] };

function streamVideo() {
    if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
        Logger.log(TAG, "getUserMedia present.");
    } else {
        Logger.log(TAG, "getUserMedia NOT present.");
        return;
    }

    navigator.mediaDevices.getUserMedia({
            video: {
                facingMode: "environment"
            },
            audio: false
        })
        .then(stream => {
            // Success
            Logger.log(TAG, "Success streaming.");

            CAMERA.stream = stream;
            CAMERA.video.srcObject = CAMERA.stream;
        })
        .catch(error => {
            // Error
            Logger.log(TAG, "ERROR streaming: " + error);
        });
}

$(document).ready(function() {
    Logger.log(TAG, "ready");

    CAMERA.initCamera();
    streamVideo();

    $("#scanned-food-list-count").on("click", function() {
        showPage("fla");
    });

    $("#btn-close").on("click", function() {
        showPage("sa");
    });

    // showPage("fla");
    showPage("sa");
});

function showPage(page) {
    Logger.log(TAG, "showPage");

    $("#scan-area").hide();
    $("#food-list-area").hide();
    $("#recipes-list-area").hide();

    if ("sa" == page) {
        populateFoodCount();
        $("#btn-close").hide();
        $("#scan-area").show();
    } else if ("fla" == page) {
        populateFoodList();
        $("#btn-close").show();
        $("#food-list-area").show();
    } else if ("rla" == page) {
        $("#btn-close").show();
        $("#recipes-list-area").show();
    } else {
        populateFoodCount();
        $("#scan-area").show();
    }
}

function populateFoodCount() {
    Logger.log(TAG, "populateFoodCount");

    let flc = CAMERA.recordedFoodStuff.length;

    if (flc > 0) {
        let flHTML = `You have <b style="color:#0d47a1">${flc}</b> food item${flc > 1? "s": ""}!`;
        $("#scanned-food-list-count").html(flHTML);
    } else {
        let flHTML = `Point camera at some food item and tap the center of the screen.`;
        $("#scanned-food-list-count").html(flHTML);
    }
}

function populateFoodList() {
    Logger.log(TAG, "populateFoodList");

    $("#food-list-area").empty();
    let flc = CAMERA.recordedFoodStuff.length;

    if (flc < 1) {
        let card = `<div class="row">
                    <div class="col s12 m12">
                        <div class="card-panel blue">
                            <center class="white-text">
                                No food items to show!
                            </center>
                        </div>
                    </div>
                    </div>`;


        $("#food-list-area").html(card);
    } else {
        let collection = `<ul class="collection with-header">`;
        collection += `<li class="collection-header"><h4>Food list</h4></li>`;

        for (let food of CAMERA.recordedFoodStuff) {
            collection += `<li class="collection-item" id="food-list-item-${food}">${food}
            <a class="secondary-content"><i class="material-icons blue-text" onclick="window.CM.removeFoodFromList('${food}');" style="cursor: pointer;">clear</i></a>
            </li>`;
        }

        collection += `<li class="collection-item"><a style="width: 100%" onclick="window.CM.formulateRecipesList();" class="waves-effect waves-light btn blue">SEARCH<i class="material-icons left">search</i></a></li></ul>`;

        $("#food-list-area").html(collection);
    }
}

function removeFoodFromList(foodID) {
    Logger.log(TAG, "removeFoodFromList");

    if (UTILS.useless(CAMERA.recordedFoodMap[foodID])) {
        return;
    }

    delete CAMERA.recordedFoodMap[foodID];
    let i = CAMERA.recordedFoodStuff.indexOf(foodID);
    CAMERA.recordedFoodStuff.splice(i, 1);
    $(`#food-list-item-${foodID}`).remove();

    let flc = CAMERA.recordedFoodStuff.length;

    if (flc < 1) {
        populateFoodList();
    }

    Logger.log(TAG, CAMERA.recordedFoodStuff);
    Logger.log(TAG, CAMERA.recordedFoodMap);
}

function showPreloader(state) {
    Logger.log(TAG, "showPreloader");

    if (state) {
        $("#preloader-area").show();
    } else {
        $("#preloader-area").hide();
    }
}

async function processImage(imageData) {
    Logger.log(TAG, "processImage");

    var subscriptionKey = "d0195af14b924a2392a1de10946f243c";
    const IMG_DATA = imageData;

    return new Promise(resolve => {
        $.ajax({
                url: `https://centralindia.api.cognitive.microsoft.com/vision/v2.0/analyze?visualFeatures=Objects&language=en`,
                beforeSend: function(xhrObj) {
                    xhrObj.setRequestHeader("Content-Type", "application/octet-stream");
                    xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
                },
                type: "POST",
                data: IMG_DATA,
                processData: false
            })
            .done(function(data) {
                Logger.log(TAG, "Successfully analyzed image.");
                Logger.log(TAG, data);
                CAMERA.recordFood(data.objects, IMG_DATA);
                resolve();
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                Logger.log(TAG, "FAILED to analyze image: " + errorThrown);
                resolve();
            });
    });
};

async function formulateRecipesList() {
    Logger.log(TAG, "formulateRecipesList");

    showPreloader(true);

    let recipes = await getRecipes(CAMERA.recordedFoodStuff);

    showPreloader(false);

    Logger.log(TAG, recipes);

    if (UTILS.useless(recipes)) {
        M.toast({ html: `Cannot retrieve recipe list :/` });
        return;
    }

    openRecipesListPage(recipes);
}

async function getRecipes(ingredients) {
    Logger.log(TAG, "getRecipes");

    if (UTILS.useless(ingredients) || ingredients.length < 1) {
        return;
    }

    let subKeyList = [
        "27b9f81cb536b3a3002a5fe7499dc816",
        "5200cfbeff7685cc56ef6a220e7f4bc2",
        "029375ff870c06203adc9cbcf05dcc8e",
        "d36db20b524230e0755bebfa2df16f33",
        "8d54f2778077be1d9550444305d5c6f8",
        "1a556973dafc247a7f993c8a488ca94e"
    ];
    // var subscriptionKey = "aebe9886c9ca7e4d554636e282dd77b2";
    var subscriptionKey = subKeyList[Math.floor(Math.random() * subKeyList.length)];
    Logger.log(TAG, `Subscription key: ${subscriptionKey}`);
    var qStr = "";

    for (let ingredient of ingredients) {
        qStr += ingredient + ",";
    }

    qStr = qStr.slice(0, qStr.length - 1);

    let params = {
        "key": subscriptionKey,
        "q": qStr
    };

    Logger.log(TAG, params);
    const URL = `https://www.food2fork.com/api/search?${$.param(params)}`;
    Logger.log(TAG, URL);

    // return new Promise(resolve => resolve(RECIPES_LIST.recipes));

    return new Promise(resolve =>
        $.ajax({
            url: URL,
            type: "GET"
        }).done(function(data) {
            Logger.log(TAG, "Successfully retrieved recipes.");
            data = JSON.parse(data);
            Logger.log(TAG, data);
            resolve(data.recipes);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            Logger.log(TAG, "FAILED to retrieve recipes: " + errorThrown);
            resolve(null);
        })
    );
}

function renderRecipeList(recipes) {
    Logger.log(TAG, "renderRecipeList");

    let collection = `<ul class="collection with-header">`;
    collection += `<li class="collection-header"><h4>Recipes</h4></li>`;
    for (let recipe of recipes) {
        collection += `
            <li class="collection-item avatar">
                <img src="${recipe.image_url}" alt="" class="circle">
                <span class="title">${recipe.title}</span>
                <p>${recipe.publisher}<br>
                <a href="${recipe.source_url}" target="_blank" class="blue-text">
                    ${recipe.source_url}
                </a>
                <a href="${recipe.source_url}" target="_blank" class="secondary-content"><i class="material-icons blue-text">open_in_new</i></a>
                </p>
            </li>
        `;
    }
    collection += `</ul>`;
    return collection;
}

function openRecipesListPage(recipes) {
    Logger.log(TAG, "openRecipesListPage");

    let recipeListUI = renderRecipeList(recipes);

    $("#recipes-list-area").html(recipeListUI);
    showPage("rla");
}

window.CM = {};
window.CM.removeFoodFromList = removeFoodFromList;
window.CM.formulateRecipesList = formulateRecipesList;